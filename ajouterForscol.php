<?php

require_once('config.php');
session_start();
$connecte = false;
    
if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Document</title>
    <style>
        a:hover{
            text-decoration:none;
            cursor: pointer;
        }
    </style>
</head>
<body>

<?php
        if(isset($_POST["ajouter"])){
            $categorie=$_POST['categorie'];
            $label = $_POST['label'];
            $prix = $_POST['prix'];
            $quantite_stock = $_POST['quantite_stock'];
            $nbrfournitures = $_POST['nbrfournitures'];
            if (!empty($categorie) && !empty($label) && !empty($prix) && !empty($quantite_stock) && !empty($nbrfournitures) && isset($_FILES['file'])){
                $tmpName = $_FILES['file']['tmp_name'];
                $image = $_FILES['file']['name'];
                move_uploaded_file($tmpName,'img/'.$image);

                $sqlState = $dbco->prepare('INSERT INTO fournitures_scolaires VALUES(null,?,?,?,?,?,?)');
                $sqlState->execute([$categorie,$label,$image,$prix,$quantite_stock,$nbrfournitures]);

                header('location:Fournitures scolaires.php');
            }
            else{
                echo "Tous les champs sont obligatoires";
            }
        }

?>
<div class="container p-3 ">
    <a class="text-success" onclick="window.history.back()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</a>
    <br>
    <h4>Ajouter une fourniture scolaire </h4>
    <form method="post" enctype="multipart/form-data">
        <div class="form-group ">
                    <label for="">Label</label>
                    <input type="text" class="form-control" id="Label" name="label" placeholder=" Entrez votre Label" >
                    <label for="">Prix</label>
                    <input type="text" class="form-control" id="prenom" name="prix" placeholder="Entrez votre prix" >
                    <label for="">quantite_stock</label>
                    <input type="text" class="form-control"  name="quantite_stock" placeholder="quantite_stock" >
                    <label for="">nbrfournitures</label>
                    <input type="text" class="form-control"  name="nbrfournitures" placeholder="nbrfournitures" >
        </div>
        <div class="custom-file">
            <label class="custom-file-label" for="customFile">Choisir un fichier</label>
            <input type="file" class="custom-file-input" name="file" id="customFile">
        </div>
        <div class="form-group">
        <label class="label" for="">categorie</label>
        <select class="form-select"name="categorie" aria-label="Default select example">
            <?php 
                $filier = $dbco->query('SELECT * FROM categorie')->fetchAll(PDO::FETCH_ASSOC);
                foreach($filier as $fil){
                    $domain = $fil['nomcatg'];
                    $idf = $fil['idcatg']; 
                    echo "<option value='$idf'>$domain</option>";
                }
            ?>
        </select>
        </div>
        <div class="d-grid gap-2">
        <button type="sumbit" class="btn btn-success" name="ajouter">AJOUTER</button>
        </div>

    </form>
</div>
<script>
    var boutonRetour = document.getElementById('retourBtn');
    boutonRetour.addEventListener('click', function() {
    window.history.back();
    });
</script>
    
</body>
</html>

