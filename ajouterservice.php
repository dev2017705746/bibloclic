<?php
require_once('config.php');
session_start();
$connecte = false;

if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
    exit();
}

if(isset($_POST["valider"])){
    $intitule = $_POST['intitule'];
    $description = $_POST['description'];
    $cout = $_POST['cout'];
    $date_disponibilite = $_POST['date_disponibilite'];

    $sql = $dbco->prepare("INSERT INTO services_informatiques (intitule, description, cout, date_disponibilite) VALUES (?, ?, ?, ?)");
    $sql->execute([$intitule, $description, $cout, $date_disponibilite]);

    header('location:service.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ajouter un service</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="css/style1.css">
    <script src="js/app.js"></script>
</head>
<body>
    <div class="signup-form">
        <form action="" method="post" id="form1" enctype="multipart/form-data">
            <a href="service.php" class="span s1"><i class="fa fa-remove"></i></a>
            <h2>Ajouter un service</h2>
            <div class="form-group">
                <label for="intitule">Intitulé</label>
                <input type="text" class="form-control" id="intitule" name="intitule" required>
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" class="form-control" id="description" name="description" required>
            </div>
            <div class="form-group">
                <label for="cout">Coût</label>
                <input type="number" class="form-control" id="cout" name="cout" required>
            </div>
            <div class="form-group">
                <label for="date_disponibilite">Date de disponibilité</label>
                <input type="date" class="form-control" id="date_disponibilite" name="date_disponibilite" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary" name="valider">Ajouter</button>
            </div>
        </form>
    </div>
</body>
</html>