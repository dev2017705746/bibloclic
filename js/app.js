function searchElements(query) {
    // Convertit la requête en minuscules pour ignorer la casse
    query = query.toLowerCase();
  
    // Parcours tous les éléments à rechercher
    $('.searchable-element').each(function() {
      var text = $(this).text().toLowerCase();
      
      // Si le texte de l'élément contient la requête, affiche l'élément
      if (text.indexOf(query) !== -1) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  }
  
      $('#search').on('input', function() {
    var query = $(this).val();
    searchElements(query);
  });