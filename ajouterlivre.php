<?php
require_once('config.php');
session_start();
$connecte = false;

if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('Location: login.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ajouter un livre</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style1.css">
</head>
<body>
<div class="signup-form">
    <form action="" method="post" enctype="multipart/form-data" id="form1">
        <a href="Livres scolaires.php" class="span s1"><i class="fa fa-remove"></i></a>
        <h2>Ajouter un livre</h2>
        <div class="form-group">
            <label for="titre">Titre</label>
            <input type="text" class="form-control" id="titre" name="titre" required>
        </div>
        <div class="form-group">
            <label for="auteur">Auteur</label>
            <input type="text" class="form-control" id="auteur" name="auteur" required>
        </div>
        <div class="form-group">
            <label for="prix">Prix</label>
            <input type="number" class="form-control" id="prix" name="prix" required>
        </div>
        <div class="form-group">
            <label for="quantite">Quantité</label>
            <input type="number" class="form-control" id="quantite" name="quantite" required>
        </div>
        <div class="form-group">
            <label for="nbrLivrevend">Nombre de livres vendus</label>
            <input type="number" class="form-control" id="nbrLivrevend" name="nbrLivrevend" required>
        </div>
        <div class="custom-file">
            <label class="custom-file-label" for="customFile">Choisir un image</label>
            <input type="file" class="custom-file-input" name="file" id="customFile" required>
        </div>
        <div class="form-group">
            <label class="label" for="">Catégorie</label>
            <select class="form-select" name="categorie" aria-label="Default select example" required>
                <?php 
                $filier = $dbco->query('SELECT * FROM categorie')->fetchAll(PDO::FETCH_ASSOC);
                foreach($filier as $fil){
                    $domain = $fil['nomcatg'];
                    $idf = $fil['idcatg']; 
                    echo "<option value='$idf'>$domain</option>";
                }
                ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary" name="add">Ajouter</button>
    </form>
</div>

<?php
if (isset($_POST['add'])){
    $categorie = $_POST['categorie'];
    $titre = $_POST['titre'];
    $auteur = $_POST['auteur'];
    $prix = $_POST['prix'];
    $quantite = $_POST['quantite'];
    $nbrLivrevend = $_POST['nbrLivrevend'];
    $image = $_FILES['file']['name'];
    $tmpName = $_FILES['file']['tmp_name'];

    move_uploaded_file($tmpName, 'img/'.$image);

    $sql = $dbco->prepare('INSERT INTO livres VALUES (null, ?, ?, ?, ?, ?, ?, ?)');
    $sql->execute([$categorie, $titre, $auteur, $prix, $quantite, $nbrLivrevend, $image]);

    header('Location: Livres scolaires.php');
    exit();
}
?>


</body>
</html>