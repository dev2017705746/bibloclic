<?php

require_once('config.php');
session_start();
$connecte = false;
    
if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
}


?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Dashboard</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="js/jquery-3.6.3.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/fonts/font-awesome.min.css">
<link rel="stylesheet" href="css/style1.css">


</head>

<body>
<div class="table-responsive">
            <div class="table-wrapper">			
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Liste <b>Catégories</b></h2>
                        </div>
                        <div class="col-sm-6">
                            <div class="search-box">
                                <div class="input-group">								
                                    <input type="text" id="search" class="form-control" placeholder="Search by Name">
                                    <span  class="input-group-addon"><i class="fa fa-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Id_Catégorie</th>
                            <th style="width: 22%; margin-left:20px;">Nom Catégorie</th>
                            <th>description</th>
                            <th>modifier</th>
                        </tr>
                    </thead>
                    <tbody>
                      
                        <?php
                         $sqlState = $dbco->query('SELECT * FROM categorie')->fetchAll(PDO::FETCH_OBJ);
             
                   foreach ($sqlState as $categorie){
            ?>
            <tr class='searchable-element'>
                <td><?= $categorie->idcatg ?></td>
                <td><a class='nav-link'  href='<?= $categorie->nomcatg ?>.php'><?= $categorie->nomcatg ?></a></td>
                <td><?= $categorie->description?></td>
               
                 <td ><a  href='ModifierCatigier.php?id=<?= $categorie->idcatg ?>'><i class="fa fa-pencil"></i></a></td>
             
            </tr>
            <?php }
            ?>
                    </tbody>
                </table>
            </div>
        </div>        
    </div>
    <script src="js/app.js"></script>
</body>
</html>