<?php
    require('config.php');
    session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Page d'Identification</title>
</head>
<style>
body {
    background-image: url('./img/biblio1.jpg'); 
    background-size: cover;
    color: #ffffff33; 
    text-align: left; 
    padding: 100px; 
  }

.container {
    margin-top: 50px;
}

.card {
    border: none;
    border-radius: 10px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

.card-header {
    background-color:  #5176c5e7;
    color: #ffffff;
    font-size: x-large;
    border-top-left-radius: 10px;
    border-top-right-radius: 100px;
    text-align: center;
    padding: 15px;
}

.card-body {
    padding: 1Opx;
    color:black
}

.form-group {
    margin-bottom: 20px;
}

label {
    font-weight: 600;
}

.form-control {
    border: 1px solid #ced4da;
    border-radius: 5px;
}

.btn-primary {
    background-color: #5176c5e7;
    border: none;
    margin-left: 36%;
}

.btn-primary:hover {
    background-color:  #5176c5e7;;
}

#forgotPasswordLink {
    text-align: center;
    display: block;
    color: #5176c5e7;
    text-decoration: none;
}

#forgotPasswordLink:hover {
    text-decoration: underline;
    
}

</style>
<body>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Autentification
                    </div>
                    <div class="card-body">
                        <form id="loginForm" action="" method="post">
                            <div class="form-group">
                                <label for="username">Login</label>
                                <input type="text" class="form-control" id="username" name="username" required  placeholder="Nom d'utilisateur">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" required placeholder="Mot de passe">
                            </div>
                            <button type="submit" class="btn btn-primary" name="btn">Se Connecter</button>
                        </form>
                        <div class="mt-3">
                            <a href="reset-password.php" id="forgotPasswordLink">Mot de passe oublié ?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  

    <?php

if(isset($_POST['btn'])){
    $user=$_POST['username'];
    $password=$_POST['password'];
    if(!empty($user)&& !empty($password)){
        $password=md5($password);

        $sqlState = $dbco->prepare('SELECT * FROM admin WHERE login=?
        AND   mot_de_passe=?');
$sqlState->execute([$user,$password]);
if($sqlState->rowCount()>=1){
        
    $_SESSION['utilisateur'] =$sqlState->fetch();
   
   header('location:index.php');
    
}else{
    ?>
    <div class="alert alert-danger" role="alert">
        Login ou password incorrectes.
    </div>
    <?php
}
}else{
?>
<div class="alert alert-danger" role="alert">
    Login , password sont obligatoires
</div>
<?php
}
}
?>
    

</body>
</html>

