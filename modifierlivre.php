<?php
require_once('config.php');
session_start();
$connecte = false;

if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
    exit();
}

$id = $_GET['id'];

$sql = $dbco->prepare("SELECT * FROM livres WHERE idlivre = ?");
$sql->execute([$id]);
$row = $sql->fetch(PDO::FETCH_ASSOC);

if (!$row) {
    echo "Livre non trouvé.";
    exit();
}

if(isset($_POST["valider"])){
    $id = $_GET['id'];
    $categorie = $_POST['categorie'];
    $titre = $_POST['titre'];
    $auteur = $_POST['auteur'];
    $prix = $_POST['prix'];
    $quantite = $_POST['quantite'];
    $nbrLivrevend = $_POST['nbrLivrevend'];

    $nouvelle_image = $_FILES['nouvelle_image']['name'];
    $chemin_image = $_FILES['nouvelle_image']['tmp_name'];

    if (!empty($nouvelle_image)) {
        $extension = pathinfo($nouvelle_image, PATHINFO_EXTENSION);
        $nom_image = time() . '.' . $extension;
        move_uploaded_file($chemin_image, 'img/' . $nom_image);
    } else {
        $nom_image = $row['photo'];
    }

    $sql = $dbco->prepare("UPDATE livres SET idcatg=?, titre=?, auteur=?, prix=?, quantite=?, nbrLivrevend=?, photo=? WHERE idlivre=?");
    $sql->execute([$categorie, $titre, $auteur, $prix, $quantite, $nbrLivrevend, $nom_image, $id]);

    header('location:Livres scolaires.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Modifier</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="css/style1.css">
    <script src="js/app.js"></script>
</head>
<body>
    <div class="signup-form">
        <form action="" method="post" id="form1" enctype="multipart/form-data">
            <a href="categorie.php" class="span s1"><i class="fa fa-remove"></i></a>
            <h2>Modifier un livre</h2>
            <div class="form-group">
                <label for="categorie">Catégorie</label>
                <select class="form-control" id="categorie" name="categorie">
                    <?php 
                    $categories = $dbco->query('SELECT * FROM categorie')->fetchAll(PDO::FETCH_ASSOC);
                    foreach($categories as $category) {
                        $categoryName = $category['nomcatg']; 
                        $categoryId = $category['idcatg']; 
                        $isSelected = ($row['idcatg'] == $categoryId) ? 'selected' : '';
                        echo "<option value='$categoryId' $isSelected>$categoryName</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="titre">Titre</label>
                <input type="text" class="form-control" id="titre" name="titre" value="<?php echo htmlspecialchars($row["titre"]); ?>" required>
            </div>
            <div class="form-group">
                <label for="auteur">Auteur</label>
                <input type="text" class="form-control" id="auteur" name="auteur" value="<?php echo htmlspecialchars($row["auteur"]); ?>" required>
            </div>
            <div classsuite de la réponse :
```html
="form-group">
                <label for="prix">Prix</label>
                <input type="number" class="form-control" id="prix" name="prix" value="<?php echo $row["prix"]; ?>" required>
            </div>
            <div class="form-group">
                <label for="quantite">Quantité</label>
                <input type="number" class="form-control" id="quantite" name="quantite" value="<?php echo $row["quantite"]; ?>" required>
            </div>
            <div class="form-group">
                <label for="nbrLivrevend">Nombre Livre Vendu</label>
                <input type="number" class="form-control" id="nbrLivrevend" name="nbrLivrevend" value="<?php echo $row["nbrLivrevend"]; ?>" required>
            </div>
            <div class="form-group">
                <label for="image">Image actuelle</label>
                <img src="img/<?php echo htmlspecialchars($row['photo']); ?>" alt="Image actuelle" style="max-width: 200px;">
            </div>
            <div class="form-group">
                <label for="nouvelle_image">Nouvelle Image (laissez vide pour conserver l'image actuelle)</label>
                <input type="file" class="form-control" id="nouvelle_image" name="nouvelle_image">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary" name="valider">Modifier</button>
            </div>
        </form>
    </div>
</body>
</html>