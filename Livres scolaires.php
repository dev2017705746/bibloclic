<?php
require_once('config.php');
session_start();
$connecte = false;

if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Liste livres</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="css/style1.css">
</head>
<style>
     img{
            width: 50px;
        }
  
</style>

<body>
<div class="table-responsive">
            <div class="table-wrapper">			
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Liste <b>Livres scolaires</b></h2>
                        </div>
                        <div class="col-sm-6">
                        <a class="text-light" href="ajouterlivre.php"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>

                            <div class="search-box">
                                <div class="input-group">								
                                    <input type="text" id="search" class="form-control" placeholder="Search by Name">
                                    <span  class="input-group-addon"><i class="fa fa-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th>Catégorie</th>
                        <th>Titre</th>
                        <th style="width: 22%; margin-left:20px;">Auteur</th>
                        <th>Photo</th>
                        <th>Prix</th>
                        <th>Quantite</th>
                        <th>Nombre Livre</th>
                        <th>modifier</th>
                        <th>Supprimer</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    

                        $sqlState = $dbco->query('SELECT * FROM livres
                         INNER JOIN categorie ON livres.idcatg = categorie.idcatg')->fetchAll(PDO::FETCH_OBJ);
                        foreach ($sqlState as $livre) {
                            ?>
                            <tr class='searchable-element'>
                            <td><?= $livre->nomcatg?></td>
                                <td><?= $livre->titre ?></td>
                                <td><?= $livre->auteur ?></td>
                                <td><img class="rounded" src="img/<?= $livre->photo ?>"></td>
                                <td><?= $livre->prix ?></td>
                                <td><?= $livre->quantite ?></td>
                                <td><?= $livre->nbrLivrevend ?></td>
                                <td><a href="modifierlivre.php?id=<?= $livre->idlivre?>"><i class="fa fa-edit"></i></a></td>
                                <td><a href="supprimerlivre.php?id=<?= $livre->idlivre ?>"  onclick="return  confirm(`Voulez vous vraiment supprimer ce livre ?`)" ><i class="fa fa-remove"></i></a></td>
                            </tr>
                        <?php
                        }
                     
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <script src="js/app.js"></script>
</body>

</html>