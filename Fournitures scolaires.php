<?php

require_once('config.php');
session_start();
$connecte = false;
    
if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
}


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="js/jquery-3.6.3.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/fonts/font-awesome.min.css">
<link rel="stylesheet" href="css/style1.css">


</head>
<style>
     img{
            width: 50px;
        }
    a{
        text-decoration:none;
    }
</style>
<body>
<div class="table-responsive">
            <div class="table-wrapper">			
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Liste <b>fournitures scolaires</b></h2>
                        </div>
                        <div class="col-sm-6">
                        <a class="text-light" href="ajouterForscol.php"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>

                            <div class="search-box">
                                <div class="input-group">								
                                    <input type="text" id="search" class="form-control" placeholder="Search by Name">
                                    <span  class="input-group-addon"><i class="fa fa-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Catégorie</th>
                            <th style="width: 22%; margin-left:20px;">Nom</th>
                            <th>Photo</th>
                            <th>Prix</th>
                            <th>Quantité stock</th>
                            <th>Nbrfourniture</th>
                            <th>Modifier</th>
                            <th>Supprimer</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
    $stagaires = $dbco->query('SELECT * from fournitures_scolaires INNER JOIN categorie ON categorie.idcatg=fournitures_scolaires.idcatg')->fetchAll(PDO::FETCH_ASSOC);
    foreach($stagaires as $stg){
        echo '<tr>
        <td>'.$stg['nomcatg'].'</td>
        <td>'.$stg['label'].'</td>
        <td><img class="rounded" src="img/'.$stg['photo'].'"></td>
        <td>'.$stg['prix'].'</td>
        <td>'.$stg['quantite_stock'].'</td>
        <td>'.$stg['nbrfournitures'].'</td>
        <td>
        <a  class="text-warning" href="modifierForscol.php?id='.$stg['idfourniture'].'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
        </td>
        <td>
        <a  class="text-danger" href="supprimerForscol.php?id='.$stg['idfourniture'].'" onclick="return  confirm(`Voulez vous vraiment supprimer ce fourniture ?`)" ><i class="fa fa-trash-o"  aria-hidden="true"></i>
        </a>
        </td>
    </tr>';
    }
?>
                    </tbody>
                </table>
            </div>
        </div>        
    </div>
    <script src="js/app.js"></script>
</body>
</html>