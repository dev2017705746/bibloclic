<?php

require_once('config.php');
session_start();
$connecte = false;
    
if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
}
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
      <?php include('./include/Sidebar.php');?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include('./include/navbar.php');
       ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                   

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Categories </div>
                                                
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
            $stmt = $dbco->query("SELECT COUNT(*) AS nbr_cat FROM categorie");
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            echo $row['nbr_cat'];            
            ?></div>
                                        </div>
                                        <div class="col-auto">
                                        <i class="fa fa-bookmark fa-2x text-gray-300" aria-hidden="true"></i>
                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            services informatiques</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
                                                            $stmt = $dbco->query("SELECT COUNT(*) AS nbr_serinfo FROM services_informatiques");
                                                            $row = $stmt->fetch(PDO::FETCH_ASSOC);
                                                            echo $row['nbr_serinfo'];            
                                                            ?></div>
                                        </div>
                                        <div class="col-auto">
                                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Livres
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php
                                                            $stmt = $dbco->query("SELECT COUNT(*) AS nbr_livre FROM livres");
                                                            $row = $stmt->fetch(PDO::FETCH_ASSOC);
                                                            echo $row['nbr_livre'];            
                                                            ?></div>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                        <i class="fa fa-book fa-2x text-gray-300" aria-hidden="true"></i>
                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            Fournitures scolaires</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
                                                            $stmt = $dbco->query("SELECT COUNT(*) AS nbr_sf FROM fournitures_scolaires");
                                                            $row = $stmt->fetch(PDO::FETCH_ASSOC);
                                                            echo $row['nbr_sf'];            
                                                            ?></div>
                                        </div>
                                        <div class="col-auto">
                                        <i class="fa fa-paperclip fa-2x text-gray-300" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Content Row -->

                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-8 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Produits les plus vendus</h6>
                                    <div class="dropdown no-arrow">
                                       
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="chart-area">
                                        <canvas id="chartProduits"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        <div class="col-xl-4 col-lg-5">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Etat des problémes</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="chart-pie pt-4 pb-8">
                                        <canvas id="chartEtatProblemes"></canvas>
                                    </div>
                                    <div class="mt-4 text-center small">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                      

                 

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; DEV201 </span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

<?php

//les produit le plus vendus
$result = $dbco->query("SELECT titre AS Nom, nbrLivrevend AS NbrVendu FROM livres UNION ALL (SELECT label AS Nom,nbrfournitures AS NbrVendu FROM fournitures_scolaires);");

$produits = array();
$ventes = array();

while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
    $produits[] = $row['Nom'];
    $ventes[] = $row['NbrVendu'];
}
// les etats de probleme
$res = $dbco->query("SELECT etat_demande, COUNT(*) AS nombre FROM    demandes_service GROUP BY  etat_demande");

$etats = array();
$nombres = array();

while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
    $etats[] = $row['etat_demande'];
    $nombres[] = $row['nombre'];
}

?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <script>
  // Récupérez le contexte des canvas
  var ctxProduits = document.getElementById('chartProduits').getContext('2d');
  var ctxEtatProblemes = document.getElementById('chartEtatProblemes').getContext('2d');

  // Définissez les données pour les deux graphiques (à personnaliser selon vos besoins)
  var dataProduits = {
    labels: <?php echo json_encode($produits); ?>,
    datasets: [
      {
        label: 'Produits les plus vendus',
        data: <?php echo json_encode($ventes); ?>,
        backgroundColor: '#36b9cc',
        borderColor: '#36b9cc',
        borderWidth: 1,
      },
    ],
  };

  var dataEtatProblemes = {
    labels: <?php echo json_encode($etats); ?>,
    datasets: [
      {
        label: 'État des problèmes',
        data: <?php echo json_encode($nombres); ?>,
        backgroundColor: ['#e74a3b', '#f6c23e', '#1cc88a'], // Personnalisez les couleurs
      },
    ],
  };

  // Créez les graphiques
  var chartProduits = new Chart(ctxProduits, {
    type: 'bar',
    data: dataProduits,
    options: {
      responsive: true,
      scales: {
        y: {
          beginAtZero: true
        }
      }
    },
  });

  var chartEtatProblemes = new Chart(ctxEtatProblemes, {
    type: 'pie', // Vous pouvez utiliser d'autres types de graphiques si vous le souhaitez
    data: dataEtatProblemes,
    options: {
      responsive: true,
    },
  });
</script>


</body>

</html>