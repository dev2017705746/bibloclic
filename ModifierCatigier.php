<?php

require_once('config.php');
session_start();
$connecte = false;
    
if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
}



$id=$_GET['id'];


$res=$dbco->prepare('SELECT * FROM categorie WHERE idcatg=?');
$res->execute([$id]);
foreach ($res->fetchAll(PDO::FETCH_ASSOC)
             as $row){
?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title> Modifier
</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="js/jquery-3.6.3.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/fonts/font-awesome.min.css">
<link rel="stylesheet" href="css/style1.css">
<script src="js/app.js"></script>

</head>
<body>
<div class="signup-form">	

   <form action="" method="post" id="form1">
   <a href="categorie.php" class="span s1"><i  class="fa fa-remove"></i></a>
       <h2>modifier un Catégorie</h2>
       <div class="form-group">
       ID: <input type="text" class="form-control" name="id_cat" value="<?php echo $row["idcatg"];?>" required="required" disabled>

           <div class="input-group">
               
           </div>
       </div>
       <div class="form-group">
       Nom: 
           <div class="input-group">
               
              <input type="text" class="form-control" name="nomcat" value="<?php echo $row["nomcatg"];?>" required="required">
           </div>
       </div>
       <div class="form-group">
       description:  
           <div class="input-group">
               
       <input type="text" class="form-control" name="descriptio" value="<?php echo $row["description"];?>" required="required">
           </div>
       </div>
       
              
       <div class="form-group">
           <button type="submit" name='upd'class="btn btn-primary btn-block btn-lg" >Save</button>  
       </div>

   </form>

</div> 
<?php }
    
if(isset($_POST['upd'])){
    $nom=$_POST['nomcat'];
    $desq=$_POST['descriptio'];
    if(!empty($nom)&&!empty($desq)){
    $sql = "UPDATE categorie SET nomcatg=?, description=? WHERE idcatg=?";
    $stmt = $dbco->prepare($sql);
    $stmt->execute([$nom, $desq,$id]);
    
    if ($stmt) {
        // Redirection ou affichage d'un message de succès
        header('Location: categorie.php');
        exit();
    } 
}
else{
    echo 'saisir tous les information';

}
}


?> 

</body>
</html>