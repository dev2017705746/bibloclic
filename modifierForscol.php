<?php
require_once('config.php');
session_start();
$connecte = false;
    
if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
}

// Code de mise à jour
if(isset($_POST["modifier"])){
    $idfourniture = $_POST['idfourniture']; // L'ID de la fourniture à mettre à jour
    $categorie = $_POST['categorie'];
    $label = $_POST['label'];
    $prix = $_POST['prix'];
    $quantite_stock = $_POST['quantite_stock'];
    $nbrfournitures = $_POST['nbrfournitures'];

    // Vérifier si une nouvelle image a été téléchargée
    $nouvelle_image = $_FILES['nouvelle_image']['name'];
    if (!empty($nouvelle_image)) {
        // Supprimer l'ancienne image s'il y en a une
        $ancienne_image = $_POST['ancienne_image'];
        if (!empty($ancienne_image)) {
            unlink('img/' . $ancienne_image);
        }
        $tmpName = $_FILES['nouvelle_image']['tmp_name'];
        move_uploaded_file($tmpName, 'img/' . $nouvelle_image);
    } else {
        // Utiliser l'ancienne image si aucune nouvelle image n'a été téléchargée
        $nouvelle_image = $_POST['ancienne_image'];
    }

    if(!empty($categorie) && !empty($label) && !empty($prix) && !empty($quantite_stock) && !empty($nbrfournitures)){
        // Mettez à jour la fourniture scolaire dans la base de données
        $sqlState = $dbco->prepare('UPDATE fournitures_scolaires SET idcatg=?, label=?, prix=?, quantite_stock=?, nbrfournitures=?, photo=? WHERE idfourniture=?');
        $sqlState->execute([$categorie, $label, $prix, $quantite_stock, $nbrfournitures, $nouvelle_image, $idfourniture]);

        header('location:Fournitures scolaires.php');
    }
    else{
        echo "Tous les champs sont obligatoires";
    }
}

// Récupération des données de la fourniture à modifier
$id = $_GET['id']; // Récupérez l'ID de la fourniture à modifier
$sqlSelect = $dbco->prepare('SELECT * FROM fournitures_scolaires WHERE idfourniture=?');
$sqlSelect->execute([$id]);
$fourniture = $sqlSelect->fetch(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Modifier une fourniture scolaire</title>
    <style>
        a:hover {
            text-decoration: none;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="container p-3 ">
        <a class="text-success" onclick="window.history.back()">
            <i class="fa fa-arrow-left" aria-hidden="true"></i> Retour
        </a>
        <br>
        <h4>Modifier une fourniture scolaire</h4>
        <form method="post" enctype="multipart/form-data">
            <input type="hidden" name="idfourniture" value="<?php echo $fourniture['idfourniture']; ?>">
            
            <div class="form-group">
                <label for="categorie">Catégorie</label>
                <select class="form-control" id="categorie" name="categorie">
                    <?php 
                        $categories = $dbco->query('SELECT * FROM categorie')->fetchAll(PDO::FETCH_ASSOC);
                        foreach($categories as $category) {
                            $categoryName = $category['nomcatg']; 
                            $categoryId = $category['idcatg']; 
                            $isSelected = ($fourniture['idcatg'] == $categoryId) ? 'selected' : '';
                            echo "<option value='$categoryId' $isSelected>$categoryName</option>";
                        }
                    ?>
                </select>
            </div>
            
            <div class="form-group">
                <label for="label">Label</label>
                <input type="text" class="form-control" id="label" name="label" value="<?php echo $fourniture['label']; ?>">
            </div>
            <div class="form-group">
                <label for="prix">Prix</label>
                <input type="text" class="form-control" id="prix" name="prix" value="<?php echo $fourniture['prix']; ?>">
            </div>
            <div class="form-group">
                <label for="quantite_stock">Quantité en stock</label>
                <input type="text" class="form-control" id="quantite_stock" name="quantite_stock"
                    value="<?php echo $fourniture['quantite_stock']; ?>">
            </div>
            <div class="form-group">
                <label for="nbrfournitures">Nombre de fournitures</label>
                <input type="text" class="form-control" id="nbrfournitures" name="nbrfournitures"
                    value="<?php echo $fourniture['nbrfournitures']; ?>">
            </div>

            <div class="form-group">
                <label for="image">Image actuelle</label>
                <img src="img/<?php echo $fourniture['photo']; ?>" alt="Image actuelle" style="max-width: 200px;">
            </div>
            <div class="form-group">
                <label for="nouvelle_image">Nouvelle Image (laissez vide si inchangée)</label>
                <input type="file" class="form-control" id="nouvelle_image" name="nouvelle_image">
            </div>

            <input type="hidden" name="ancienne_image" value="<?php echo $fourniture['photo']; ?>">
            
            <div class="d-grid gap-2">
                <button type="submit" class="btn btn-success" name="modifier">Modifier</button>
            </div>
        </form>
    </div>
</body>
</html>
