<?php
require_once('config.php');
session_start();
$connecte = false;

if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
}

$id=$_GET['id'];

$sqlState = $dbco->prepare("delete from services_informatiques where idservice=?");
$sqlState->execute([$id]);
if($sqlState){
    header('location:service.php');
}