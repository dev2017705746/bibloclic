 <!-- Sidebar -->
 
 
 <?php
// Obtenez le nom de la page courante
$current_page = basename($_SERVER['SCRIPT_NAME']);
?>
 
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->


<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item <?= ($current_page == 'index.php') ? 'active' : '' ?>">
    <a class="nav-link" href="index.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item <?= ($current_page == 'categorie.php') ? 'active' : '' ?>">
    <a class="nav-link collapsed" href="categorie.php" data-toggle="collapse" data-target="#collapseTwo"
        aria-expanded="true" aria-controls="collapseTwo">
        <i class="fa fa-bookmark" aria-hidden="true"></i>
        <span>Categories</span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Les categories:</h6>
            <a class="collapse-item" href="categorie.php">Livres scolaires</a>
            <a class="collapse-item" href="categorie.php">Fournitures scolaires</a>
        </div>
    </div>
</li>



<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->


<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item <?= ($current_page == 'service_info.php') ? 'active' : '' ?>">
    <a class="nav-link collapsed" href="service_info.php" data-toggle="collapse" data-target="#collapsePages"
        aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-clipboard-list "></i>
        <span>Services informatiques</span>
    </a>
    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Les services informatiques</h6>
            <a class="collapse-item" href="service.php">Réparation matérielle</a>
            <a class="collapse-item" href="service.php">Assistance logicielle</a>
           
    </div>
</li>
<hr class="sidebar-divider">

<!-- Nav Item - Charts -->
<li class="nav-item <?= ($current_page == 'personne.php') ? 'active' : '' ?>">
    <a class="nav-link" href="personne.php">
    <i class="fa fa-users" aria-hidden="true"></i>
        <span>Personnes</span></a>
</li>
<hr class="sidebar-divider">
<!-- Nav Item - Tables -->
<li class="nav-item <?= ($current_page == 'profile.php') ? 'active' : '' ?>">
    <a class="nav-link" href="profile.php">
        <i class="fas fa-user fa-fw"></i>
        <span>Profile</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<li class="nav-item">
    <a class="nav-link" href="deconnexion.php">
        <i class="fas fa-sign-out-alt fa-sm fa-fw "></i>
        <span>Déconnecter</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>
</ul>