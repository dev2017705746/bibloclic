<?php

require('config.php');
session_start();

if (isset($_SESSION['utilisateur'])) {
    $admin = $_SESSION['utilisateur'];

    if (isset($_POST['valider'])) {
        $errors = [];
        $nom = trim($_POST['nom']);
        $prenom = trim($_POST['prenom']);
        $login = trim($_POST['login']);
        $email = trim($_POST['email']);
        $NouveauMotDePasse = $_POST['Nmot_de_passe'];
        $confirmationMotDePasse = $_POST['mot_de_passeconfirm'];

        // Validation des champs
        if (empty($nom)) {
            $errors['nom'] = true;
        }
        if (empty($prenom)) {
            $errors['prenom'] = true;
        }
        if (empty($login)) {
            $errors['login'] = true;
        }
        if (empty($email)) {
            $errors['email'] = true;
        }
     
        if ($NouveauMotDePasse !== $confirmationMotDePasse) {
            $errors['password'] = true;
        }

        if (empty($errors)) {
            // Mise à jour des données de l'administrateur dans la session
            $admin['nom'] = $nom;
            $admin['prenom'] = $prenom;
            $admin['login'] = $login;
            $admin['email'] = $email;

            if (!empty($NouveauMotDePasse)) {
                // Mettez à jour le mot de passe uniquement si un nouveau mot de passe a été fourni
                $admin['mot_de_passe'] = md5($NouveauMotDePasse);
            }

            

        // Gestion de l'envoi de la nouvelle photo
        if (!empty($_FILES['picture']['name'])) {
            $file = $_FILES['picture'];

            // Validez le fichier et traitez-le
            $allowedExtensions = ['jpg', 'jpeg', 'png'];
            $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);

            if (!in_array(strtolower($fileExtension), $allowedExtensions) || $file['size'] > 2000000) {
                $errors['picture'] = true;
            } else {
                $newFileName = uniqid('profile_img_') . '.' . $fileExtension;
                if (move_uploaded_file($file['tmp_name'], 'img/' . $newFileName)) {
                    // Mettez à jour le nom de la photo dans la base de données alami10.ahmed99@gmail.com
                    $admin['photo'] = $newFileName;
                } else {
                    $errors['upload'] = true;
                }
            }
        }
            // Mettez à jour la session avec les nouvelles informations de l'administrateur
            $_SESSION['utilisateur'] = $admin;
            // Mettez à jour les données de l'administrateur dans la base de données
    $sql = "UPDATE admin SET nom=?, prenom=?, login=?, email=?, mot_de_passe=?, tele=?, photo=? WHERE idadmin=?";
    $stmt = $dbco->prepare($sql);
    $stmt->execute([$admin['nom'], $admin['prenom'], $admin['login'], $admin['email'], $admin['mot_de_passe'], $admin['tele'], $admin['photo'], $admin['idadmin']]);
    
    if ($stmt) {
        // Redirection ou affichage d'un message de succès
        header('Location: profile.php');
        exit();
    } else {
        $errors['db'] = true;
    }

            // Redirection ou affichage d'un message de succès
            // ...
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Metadonnées, liens CSS, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Profile Admin</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    
</head>
<style>
     .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
        
            border-radius: 10px;
        }
         .row {
            display: flex;
            flex-wrap: wrap;
        }
        #pass{
            margin-left:-15%;
        }
        .col-6 {
            flex: 0 0 50%;
            max-width: 50%;
            padding: 2px;
        }
        img {
            max-width: 150px;
            max-height: 150px;
            border: 2px solid #ccc;
            border-radius: 50%;
        }
        .form-control {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        .form-group {
            margin-bottom: 10px;
        }

        label {
            font-weight: bold;
            margin-left:-55%;
        }
        .invalid-feedback {
            color: #ff0000;
        }
         /* Style the submit button */
         .btn-primary {
            background-color:#4e73df;
            color: #fff;
            padding: 10px 20px;
            margin-top:5%;
            width: 75%;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
</style>


<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include('./include/Sidebar.php'); ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include('./include/navbar.php'); ?>
                <div class="container  bg-white-600 static-top shadow">
                    <h1 class="text-center">Modifier l'administrateur</h1>
                    <form action="" method="POST" enctype="multipart/form-data" class='form'>
                    <div class="form-group">
                             <input type="hidden" class="form-control" name="admin_id" value="<?=  $admin['idadmin']?>" readonly>

                             </div>
                            
                             <div class="container text-center">
  <div class="row g-2">
    <div class="col-6">
      <div class="p-3">  <label for="nom">Nom</label>
                <input type="text" class="form-control <?= isset($errors['nom']) ? 'is-invalid' : '' ?>" name="nom"
                    value="<?= $admin['nom'] ?>">
                <?php if (isset($errors['nom'])) : ?>
                <div class="invalid-feedback">Le nom ne peut pas être vide.</div>
                <?php endif; ?></div>
    </div>
    <div class="col-6">
      <div class="p-3"> <label for="prenom mt-2">Prénom </label>
                <input type="text" class="form-control <?= isset($errors['prenom']) ? 'is-invalid' : '' ?>"
                    name="prenom" value="<?= $admin['prenom'] ?>">
                <?php if (isset($errors['prenom'])) : ?>
                <div class="invalid-feedback">Le prenom ne peut pas être vide.</div>
                <?php endif; ?></div>
    </div>
    <div class="col-6">
      <div class="p-3"><label for="login">Login </label>
                <input type="text" class="form-control <?= isset($errors['login']) ? 'is-invalid' : '' ?>" name="login"
                    value="<?= $admin['login'] ?>">
                <?php if (isset($errors['login'])) : ?>
                <div class="invalid-feedback">Le login ne peut pas être vide.</div>
                <?php endif; ?></div>
    </div>
    <div class="col-6">
      <div class="p-3">  <label for="email">Email </label>
                <input type="email" class="form-control <?= isset($errors['email']) ? 'is-invalid' : '' ?>" name="email"
                    value="<?= $admin['email'] ?>"  >
                <?php if (isset($errors['email'])) : ?>
                <div class="invalid-feedback">L'email ne peut pas être vide.</div>
                <?php endif; ?></div>
    </div>


    <div class="col-6">
      <div class="p-3"> <div class="form-group">
                <label for="Nmot_de_passe">Mot de passe </label>
                <input type="password" class="form-control "
                    name="mot_de_passe"placeholder="Votre mot de passe" value='<?= md5($admin['mot_de_passe'])?>'readonly >
                    </div></div>
    </div>
    <div class="col-6">
      <div class="p-3"> <label for="tele">Téléphone </label>
                <input type="tel" class="form-control <?= isset($errors['tele']) ? 'is-invalid' : '' ?>" name="tele"
                    value="<?= $admin['tele']?>">
                <?php if (isset($errors['tele'])) : ?>
                <div class="invalid-feedback">Le numéro de téléphone n'est pas valide.</div>
                <?php endif; ?></div>
    </div>
    <div class="col-6">
      <div class="p-3"><label id="pass"for="Nmot_de_passe">Nouveau mot de passe :</label>
                <input type="password" class="form-control <?= isset($errors['password']) ? 'is-invalid' : '' ?>"
                    name="Nmot_de_passe"placeholder="Votre mot de passe">
                <?php if (isset($errors['password'])) : ?>
                <div class="invalid-feedback">Les mots de passe ne correspondent pas.</div>
                <?php endif; ?></div>
    </div>
    <div class="col-6">
      <div class="p-3"> <label id='pass'for="mot_de_passeconfirm">Confirmer mot de passe </label>
                <input type="password" class="form-control <?= isset($errors['password']) ? 'is-invalid' : '' ?>"
                    name="mot_de_passeconfirm"placeholder="Retapez le mot de passe">
                <?php if (isset($errors['password'])) : ?>
                <div class="invalid-feedback"></div>
                <?php endif; ?></div>
    </div>

    
</div>
<div class="">
                <div class="flis">
                <img class="rounded-circle mx-auto d-block" src="img/<?= $admin['photo'] ?>"
                     alt="Photo de profil"><br>
                <input class="ml-5" type="file" name="picture" id="avatar">
                </div>
                <?php if (isset($errors['picture'])) : ?>
                <div class="text-danger">Erreur de téléchargement : Assurez-vous que l'image est au format jpg, jpeg ou
                    png et ne dépasse pas 2 Mo.</div>
                <?php endif; ?>
            </div>
            <button type="submit" class="btn btn-primary mx-auto d-block" name="valider">Modifier</button>
  </div>

        </form>
    </div>
    </div>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
    
</body>

</html>