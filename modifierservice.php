<?php
require_once('config.php');
session_start();
$connecte = false;

if (empty($_SESSION['utilisateur'])) {
    $connecte = true;
    header('location:login.php');
    exit();
}

$id = $_GET['id'];

$sql = $dbco->prepare("SELECT * FROM services_informatiques WHERE idservice = ?");
$sql->execute([$id]);
$row = $sql->fetch(PDO::FETCH_ASSOC);

if (!$row) {
    echo "services_informatiques non trouvé.";
    exit();
}

if(isset($_POST["valider"])){
    $idservice = $_GET['id'];
    $intitule = $_POST['intitule'];
    $description = $_POST['description'];
    $cout = $_POST['cout'];
    $date_disponibilite = $_POST['date_disponibilite'];

    $sql = $dbco->prepare("UPDATE services_informatiques SET intitule=?, description=?, cout=?, date_disponibilite=? WHERE idservice=?");
    $sql->execute([$intitule, $description, $cout, $date_disponibilite, $idservice]);

    header('location:service.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Modifier</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="css/style1.css">
    <script src="js/app.js"></script>
</head>
<body>
    <div class="signup-form">
        <form action="" method="post" id="form1" enctype="multipart/form-data">
            <a href="service.php" class="span s1"><i class="fa fa-remove"></i></a>
            <h2>Modifier un service</h2>
            <div class="form-group">
                ID: <input type="text" class="form-control" name="id_serv" value="<?php echo $row["idservice"];?>" required="required" disabled>
            </div>
            <div class="form-group">
                <label for="intitule">intitule</label>
                <input type="text" class="form-control" id="intitule" name="intitule" value="<?php echo htmlspecialchars($row["intitule"]); ?>" required>
            </div>
            <div class="form-group">
                <label for="description">description</label>
                <input type="text" class="form-control" id="description" name="description" value="<?php echo htmlspecialchars($row["description"]); ?>" required>
            </div>
            <div class="form-group">
                <label for="cout">cout</label>
                <input type="number" class="form-control" id="cout" name="cout" value="<?php echo $row["cout"]; ?>" required>
            </div>
            <div class="form-group">
                <label for="date_disponibilite">date_disponibilite</label>
                <input type="date" class="form-control" id="date_disponibilite" name="date_disponibilite" value="<?php echo $row["date_disponibilite"]; ?>" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary" name="valider">Modifier</button>
            </div>
        </form>
    </div>
</body>
</html>